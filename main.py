from orm import DEFAULT_SESSION_FACTORY, start_mappers
from model import Onboarding
from sqlalchemy.types import Unicode


def main():
    start_mappers()

    o = Onboarding(
        organization="uma organização",
        skill="reply",
        username="um username",
        channel="messenger",
        brand_id="um marca",
        authorizations={"uma chave": "uma chave engraçada!!!"},
        config={"uma chave": "uma chave engraçada!!!"},
        booking={},
    )
    session = DEFAULT_SESSION_FACTORY()
    session.add(o)
    session.commit()

    for x in session.query(Onboarding).all():
        print('all', x)

    print()

    for x in (
        session.query(Onboarding)
        .filter(
            Onboarding.config["uma chave"].astext.cast(Unicode)
            == "uma chave engraçada!!!"
        )
        .all()
    ):
        print('query by config', x)

    print()

    for x in (
        session.query(Onboarding)
        .filter(
            Onboarding.authorizations["uma chave"].astext.cast(Unicode)
            == "uma chave engraçada!!!"
        )
        .all()
    ):
        print('query by authorizations', x)


if __name__ == "__main__":
    main()
