"""ORM"""
from typing import Any

from sqlalchemy_utils.types.json import JSONType
from sqlalchemy import (
    ARRAY,
    JSON,
    TIMESTAMP,
    BigInteger,
    Boolean,
    Column,
    Enum,
    Float,
    ForeignKey,
    Integer,
    Table,
    Text,
    create_engine,
    event,
)
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import registry, sessionmaker

from model import Onboarding

from sqlalchemy_utils import EncryptedType


host = 'localhost'
port = '5432'
database = 'cryptopg'
user = 'postgres'
password = 'password'
encryption_key = 'some-random-string'

engine = create_engine(f'postgresql://{user}:{password}@{host}:{port}/{database}')

DEFAULT_SESSION_FACTORY = sessionmaker(bind=engine, expire_on_commit=False)

mapper_registry = registry()

onboardings = Table(
    "onboardings",
    mapper_registry.metadata,
    Column("organization_id", BigInteger, autoincrement=True, primary_key=True),
    Column("organization", EncryptedType(Text, key=encryption_key)),
    Column("skill", Text),
    Column("channel", Text),
    Column("brand_id", Text),
    Column("username", Text),
    Column("authorizations", EncryptedType(JSONType, key=encryption_key)),
    Column("config", JSONB),
    Column("booking", JSON),
    Column("created_time", TIMESTAMP(timezone=True)),
    Column("production_ready", Boolean, default=False),
    schema="public",
)

mapper_registry.metadata.create_all(engine)

def start_mappers() -> None:
    """Map models to DB"""
    mapper_registry.map_imperatively(
        Onboarding,
        onboardings,
    )
