from dataclasses import dataclass, field
from datetime import datetime


@dataclass
class Onboarding:
    organization: str
    skill: str
    channel: str
    brand_id: str
    username: str
    authorizations: dict
    config: dict
    booking: dict
    production_ready: bool = False
    created_time: datetime = field(default_factory=datetime.utcnow)
    organization_id: int = None