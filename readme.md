Create the container:

```shell
docker run --name cryptopg -p 5432:5432  -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=password -e POSTGRES_DB=cryptopg postgres
```
 
Install the requirements from `requirements.txt`

Run:

```shell
python main.py
```

It will output the column `organization` as a readable string.

Run `psql -h localhost -p 5432 -U postgres -W cryptopg`, then:

```sql
select * from onboarding;
```

The column `organization` will be shown in a binary format.